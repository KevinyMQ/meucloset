import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:meu_closet/models/item.dart';
import 'package:meu_closet/models/my_user.dart';
import 'package:meu_closet/services/auth.dart';
import 'package:provider/provider.dart';

class ItensList extends StatelessWidget {
  Function funca;
  BuildContext contexta;
  String str;
  ItensList({this.str, this.funca, this.contexta});


  @override
  Widget build(BuildContext context) {

    print('meu texto é: ${str}');
    final AuthService _auth = AuthService();

    final tiles = Provider.of<List<Item>>(context) ?? [];
    final user = Provider.of<MyUser>(context);

    return ListView.builder(
      shrinkWrap: true,
      itemCount: tiles.length,
      itemBuilder: (context, index) {

        if(tiles[index].uidOwner == user.uid) {
          DateTime date = DateTime.parse(tiles[index].date);

          return Container(
              //height: double.infinity,
              //padding: EdgeInsets.fromLTRB(20,10,20,40),
              child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Container(
                      height: 100,
                      padding: EdgeInsets.all(4),
                      color: Colors.grey[200],
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Container(
                              margin: EdgeInsets.only(right: 5),
                              child: Image.asset('assets/pieces/${tiles[index].category}.jpg', width: 90,)
                          ),
                          Expanded(
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.start,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text(
                                  '${tiles[index].description}',
                                  overflow: TextOverflow.clip,
                                  style: TextStyle(
                                      fontSize: 16,
                                      fontWeight: FontWeight.bold
                                  ),
                                ),
                                Text('${tiles[index].category}\n'),
                                Row(
                                  children: [
                                    Column(
                                      crossAxisAlignment: CrossAxisAlignment.start,
                                      children: [
                                        Text('Valor compra:'),
                                        Text(
                                          '${tiles[index].buyPrice} R\$',
                                          style: TextStyle(
                                              fontSize: 22
                                          ),
                                        ),
                                      ],
                                    ),
                                    SizedBox(
                                      width: 10,
                                    ),
                                    Column(
                                      crossAxisAlignment: CrossAxisAlignment.start,
                                      children: [
                                        Text('Valor venda:'),
                                        Text(
                                          '${tiles[index].sellPrice} R\$',
                                          style: TextStyle(
                                              fontSize: 22
                                          ),
                                        ),
                                      ],
                                    ),
                                  ],
                                )
                              ],
                            ),
                          ),
                          Column(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              GestureDetector(
                                onTap: (){

                                },
                                child: Container(
                                  child: Icon(Icons.delete_forever_rounded),
                                ),
                              ),
                              GestureDetector(
                                onTap: (){
                                  print('copie');
                                  funca(contexta, '${tiles[index].id}');
                                },
                                child: Container(
                                  child: Icon(Icons.cached),
                                ),
                              )
                            ],
                          )
                        ],
                      ),
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [

                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            // Container(
                            //   child: Text(
                            //     'User:\n${tiles[index].userUid}',
                            //     style: TextStyle(
                            //         fontWeight: FontWeight.bold
                            //     ),
                            //   ),
                            // ),

                          ],
                        ),

                        // FlatButton(
                        //   onPressed: () {
                        //
                        //   },
                        //   child: Text(
                        //     'Checar',
                        //     style: TextStyle(
                        //         color: Colors.white
                        //     ),
                        //   ),
                        //   color: Colors.blue,
                        // )
                      ],
                    ),
                    Divider(
                      color: Colors.black,
                    )
                  ]
              )

          );
        }
        return Container();
      },
    ) ;

  }
}
