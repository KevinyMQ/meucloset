import 'dart:math';

class MyUser {
  final String uid;
  String picture;
  String address;
  String phone;
  String email;
  String password;

  MyUser({this.uid});

  String generateRandomString(int len) {
    var r = Random();
    const _chars = 'AaBbCcDdEeFfGgHhIiJjKkLlMmNnOoPpQqRrSsTtUuVvWwXxYyZz1234567890';
    return List.generate(len, (index) => _chars[r.nextInt(_chars.length)]).join();
  }

}