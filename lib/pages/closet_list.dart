import 'package:floating_action_bubble/floating_action_bubble.dart';
import 'package:flutter/material.dart';
import 'package:meu_closet/models/item.dart';
import 'package:meu_closet/services/auth.dart';
import 'package:meu_closet/services/database.dart';
import 'package:meu_closet/widgets/itens_list.dart';
import 'package:provider/provider.dart';
import 'package:meu_closet/models/my_user.dart';
import 'package:clipboard/clipboard.dart';

class ClosetList extends StatefulWidget {
  @override
  _ClosetListState createState() => _ClosetListState();
}

class _ClosetListState extends State<ClosetList> with SingleTickerProviderStateMixin{

  Animation<double> _animation;
  AnimationController _animationController;
  final AuthService _auth = AuthService();
  String codeItem;
  @override
  void initState(){

    _animationController = AnimationController(
      vsync: this,
      duration: Duration(milliseconds: 260),
    );

    final curvedAnimation = CurvedAnimation(curve: Curves.easeInOut, parent: _animationController);
    _animation = Tween<double>(begin: 0, end: 1).animate(curvedAnimation);

    super.initState();


  }
  
  Container cont = Container(
    height: 100,
    color: Colors.grey,
    child: Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Container(
            child: Image.network('https://picsum.photos/250?image=9')
        ),
        Expanded(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                'Descricao descricao de scricaossss s s s sss ssssssssss.',
                overflow: TextOverflow.clip,
                style: TextStyle(
                    fontSize: 16,
                    fontWeight: FontWeight.bold
                ),
              ),
              Text('Categoria'),
              Row(
                children: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text('Valor de compra:'),
                      Text(
                        '40,00 R\$',
                        style: TextStyle(
                            fontSize: 22
                        ),
                      ),
                    ],
                  ),
                  SizedBox(
                    width: 20,
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text('Valor de venda:'),
                      Text(
                        '40,00 R\$',
                        style: TextStyle(
                            fontSize: 22
                        ),
                      ),
                    ],
                  ),
                ],
              )
            ],
          ),
        ),
        Column(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            GestureDetector(
              onTap: (){

              },
              child: Container(
                child: Icon(Icons.delete_forever_rounded),
              ),
            ),
            GestureDetector(
              onTap: (){

              },
              child: Container(
                child: Icon(Icons.cached),
              ),
            )
          ],
        )
      ],
    ),
  );

  createAlertDialog(BuildContext context, String code) {
    TextEditingController customController = TextEditingController();
    return showDialog(
        context: context,
        builder: (context) {
          return AlertDialog(
            title: Text('Código do item'),
            content: SingleChildScrollView(
              physics: NeverScrollableScrollPhysics(),
              child: Column(
                mainAxisSize: MainAxisSize.min,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    '$code',
                    style: TextStyle(
                      color: Colors.purple,
                      fontWeight: FontWeight.bold,
                      fontSize: 20
                    ),
                  ),
                  Text(
                      '\nVocê pode compartilhar o código com alguém para que vejam seu item.',
                    style: TextStyle(
                        color: Colors.grey[600],
                        fontSize: 12
                    ),
                  )

                ],
              ),
            ),
            actions: [
              MaterialButton(
                elevation: 5.0,
                child: Text('Copiar'),
                onPressed: () async {
                  FlutterClipboard.copy('$code').then(( value ) => print('copied'));
                  Navigator.pop(context);
                  // dynamic resp = await _auth.signInWithEmailAndPasswordUpdate(
                  //     new_email, new_password, birthday, civilState, schoolInstitute);
                  //
                  // print('Email: $new_email');
                  // print('Senha: $new_password');
                  // print('Repetir Senha: $repeat_password');
                  setState(() {
                    // error = resp;
                  });
                  // print('ERRO: $resp');
                  // if(resp == '0'){
                  //   Navigator.of(context, rootNavigator: true).pop('dialog');
                  // }
                },
              )
            ],
          );
        });
  }

  insertCode(BuildContext context) {
    TextEditingController customController = TextEditingController();
    return showDialog(
        context: context,
        builder: (context) {
          return AlertDialog(
            title: Text('Insira código'),
            content: SingleChildScrollView(
              physics: NeverScrollableScrollPhysics(),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                mainAxisSize: MainAxisSize.min,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  TextField(
                    onChanged: (val) {
                      setState(() {
                        codeItem = val;
                      });
                    },
                    enableSuggestions: false,
                    autocorrect: false,
                    decoration: InputDecoration(
                      hintText: 'Digitar aqui',
                      focusColor: Colors.blue[900],
                    ),
                  ),
                ],
              ),
            ),
            actions: [
              MaterialButton(
                elevation: 5.0,
                child: Text('Buscar'),
                onPressed: () async {
                  Item item = await DatabaseService().getItemSearch(codeItem);
                  Navigator.pop(context);
                  showItemSearch(context, item);
                  // dynamic resp = await _auth.signInWithEmailAndPasswordUpdate(
                  //     new_email, new_password, birthday, civilState, schoolInstitute);
                  //
                  // print('Email: $new_email');
                  // print('Senha: $new_password');
                  // print('Repetir Senha: $repeat_password');
                  setState(() {
                    // error = resp;
                  });
                  // print('ERRO: $resp');
                  // if(resp == '0'){
                  //   Navigator.of(context, rootNavigator: true).pop('dialog');
                  // }
                },
              )
            ],
          );
        });
  }

  showItemSearch(BuildContext context, Item item) {
    return showDialog(
        context: context,
        builder: (context) {
          return AlertDialog(
            title: Text('Item:\n${item.id}'),
            content: SingleChildScrollView(
              physics: NeverScrollableScrollPhysics(),
              child: Column(
                mainAxisSize: MainAxisSize.min,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text('Descricão:\n${item.description}'),
                  Text('Categoria:\n${item.category}'),
                  Text('Preço de venda:\n${item.sellPrice}'),
                  Text('Preço de compra:\n${item.buyPrice}'),
                ],
              ),
            ),
            actions: [
              MaterialButton(
                elevation: 5.0,
                child: Text('Okay'),
                onPressed: () async {



                  Navigator.pop(context);
                  // dynamic resp = await _auth.signInWithEmailAndPasswordUpdate(
                  //     new_email, new_password, birthday, civilState, schoolInstitute);
                  //
                  // print('Email: $new_email');
                  // print('Senha: $new_password');
                  // print('Repetir Senha: $repeat_password');
                  setState(() {
                    // error = resp;
                  });
                  // print('ERRO: $resp');
                  // if(resp == '0'){
                  //   Navigator.of(context, rootNavigator: true).pop('dialog');
                  // }
                },
              )
            ],
          );
        });
  }

  @override
  Widget build(BuildContext context) {

    final user = Provider.of<MyUser>(context);


    return StreamProvider<List<Item>>.value(
    value: DatabaseService().items,
    child: Scaffold(
      backgroundColor: Colors.purple,
      appBar: AppBar(
        title: Text('Closet'),
        actions: <Widget>[
          FlatButton.icon(
            icon: Icon(
              Icons.person,
              color: Colors.white,
            ),
            label: Text(
              'Sair',
              style: TextStyle(color: Colors.white),
            ),
            onPressed: () async {
              await _auth.signOut();
              Navigator.pushReplacementNamed(context, '/');
            },
          )
        ],
      ),
      body: Container(
        height: double.infinity,
        margin: EdgeInsets.fromLTRB(15, 0 ,15, 10),
        padding: EdgeInsets.fromLTRB(10, 10, 10, 10),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.only(bottomRight: Radius.circular(100)),
          color: Colors.white,
          border: Border.all(width: 0, color: Colors.white),
        ),
        child:
        // Column(
        //   children: [
        //     FlatButton(
        //       onPressed: (){
        //         //createAlertDialog(context, 'saddsadawsdsa');
        //         print('O contexto é: $context');
        //       },
        //       child: Text('sadasds'),
        //       color: Colors.blue,
        //       height: 100,
        //     ),
        //     //ItensList(func: createAlertDialog, fromContext: context),
        //   ],
        // )
        ItensList(contexta: context, funca: createAlertDialog,),

    ),

        floatingActionButtonLocation: FloatingActionButtonLocation.endFloat,

        //Init Floating Action Bubble
        floatingActionButton: FloatingActionBubble(
          // Menu items
          items: <Bubble>[

            // Floating action menu item
            Bubble(
              title:"Nova roupa",
              iconColor :Colors.white,
              bubbleColor : Colors.blue,
              icon:Icons.playlist_add,
              titleStyle:TextStyle(fontSize: 16 , color: Colors.white),
              onPress: () {
                _animationController.reverse();
                Navigator.pushNamed(context, '/add');
              },
            ),
            // Floating action menu item
            Bubble(
              title:"Inserir código",
              iconColor :Colors.white,
              bubbleColor : Colors.blue,
              icon:Icons.search,
              titleStyle:TextStyle(fontSize: 16 , color: Colors.white),
              onPress: () {
                _animationController.reverse();
                insertCode(context);
              },
            ),
          ],

          // animation controller
          animation: _animation,


          // On pressed change animation state
          onPress: (){
            //_animationController.reverse();
            _animationController.isCompleted
                 ? _animationController.reverse()
                 : _animationController.forward();
          },
            // _animationController.isCompleted
            //     ? _animationController.reverse
            //     : _animationController.forward,

          // Floating Action button Icon color
          iconColor: Colors.blue,

          // Flaoting Action button Icon
          icon: AnimatedIcons.menu_close,
        )
    ));
  }
}
