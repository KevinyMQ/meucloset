import 'package:flutter/material.dart';
import 'package:meu_closet/models/my_user.dart';
import 'package:meu_closet/services/auth.dart';



class Login extends StatefulWidget {
  @override
  _LoginState createState() => _LoginState();
}

class _LoginState extends State<Login> {

  String email;
  String password;

  final AuthService _auth = AuthService();
  bool passwordVisible = true;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.deepPurple,
      body: SafeArea(
          child: Center(
            child: SingleChildScrollView(
              child: Container(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Container(
                      child: Text(
                        'Meu Closet',
                        style: TextStyle(
                          fontSize: 34,
                          fontWeight: FontWeight.bold,
                          color: Colors.white
                        ),
                      ),
                    ),
                    Container(
                      margin: EdgeInsets.all(20),
                      padding: EdgeInsets.all(20),
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.only(bottomLeft: Radius.circular(40), bottomRight: Radius.circular(40), topLeft: Radius.circular(40), topRight: Radius.circular(40)),
                        color: Colors.white,
                        border: Border.all(width: 0, color: Colors.white),
                      ),
                      child: Column(
                        children: [
                          Container(
                            child: Column(
                              children: [
                                Container(
                                  child: Text(
                                    'LOGIN',
                                    style: TextStyle(
                                      fontSize: 24
                                    ),
                                  ),
                                ),
                                Container(
                                  child: Column(
                                    children: [
                                      TextField(
                                        onChanged: (val) {
                                          setState(() {
                                            email = val;
                                          });
                                        },
                                        decoration: InputDecoration(hintText: 'Digitar email',),
                                      ),
                                      TextField(
                                        onChanged: (val) {
                                          setState(() {
                                            password = val;
                                          });
                                        },
                                        obscureText: passwordVisible,
                                        enableSuggestions: false,
                                        autocorrect: false,
                                        decoration: InputDecoration(
                                          hintText: 'Digitar senha',
                                          focusColor: Colors.blue[900],
                                          suffixIcon: IconButton(
                                            icon: Icon(
                                              // Based on passwordVisible state choose the icon
                                              passwordVisible == true ? Icons.visibility : Icons.visibility_off,
                                              color: Theme.of(context).primaryColorDark,
                                            ),
                                            onPressed: () {
                                              // Update the state i.e. toogle the state of passwordVisible variable
                                              setState(() {
                                                passwordVisible = !passwordVisible;
                                              });
                                            },
                                          ),
                                        ),
                                      ),
                                      SizedBox(height: 20,),
                                      FlatButton(
                                        minWidth: double.infinity,
                                        height: 60,
                                        shape: RoundedRectangleBorder(borderRadius: BorderRadius.only(bottomLeft: Radius.circular(40), bottomRight: Radius.circular(40)),),
                                        onPressed: () async {
                                          dynamic result = await _auth.signInWithEmailAndPassword(email, password);
                                          if (result.runtimeType == MyUser) {
                                          Navigator.pushNamedAndRemoveUntil(context, '/closet_list', (Route<dynamic> route) => false);
                                          } else {
                                          print("login erro ${result}");
                                          setState(() {
                                          //error = result;
                                          });
                                          }
                                        },
                                        child: Row(
                                          mainAxisSize: MainAxisSize.min,
                                          children: [
                                            Text(
                                              'ENTRAR',
                                              style: TextStyle(
                                                color: Colors.white,
                                              ),
                                            ),
                                            Icon(
                                              Icons.cached,
                                              color: Colors.white,
                                            ),
                                          ],
                                        ),
                                        color: Colors.purple,
                                      ),
                                    ],
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ],
                      ),
                    ),
                    Container(
                      margin: EdgeInsets.symmetric(horizontal: 20),
                      alignment: Alignment.center,
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        //mainAxisSize: MainAxisSize.min,
                        children: [
                          Text(
                            'Cadastre seus dados e crie sua conta\nno aplicativo se ainda não tem uma.',
                            style: TextStyle(
                              fontSize: 14,
                              color: Colors.white
                            ),
                          ),
                          FlatButton(
                            onPressed: (){
                              Navigator.pushReplacementNamed(context, '/register');
                            },
                            child: Row(
                              children: [
                                Text(
                                  'Cadastrar',
                                ),
                                Icon(Icons.ac_unit)
                              ],
                            ),
                            color: Colors.white,
                          )
                        ],
                      ),
                    )

                  ],
                ),
              ),
            ),
          )
      ),
    );
  }
}
