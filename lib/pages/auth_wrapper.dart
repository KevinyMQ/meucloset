import 'package:flutter/material.dart';
import 'package:meu_closet/models/my_user.dart';
import 'package:meu_closet/pages/closet_list.dart';
import 'package:meu_closet/pages/login.dart';
import 'package:provider/provider.dart';

class AuthWrapper extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final user = Provider.of<MyUser>(context);
    if(user == null) {
      return Login();
    }else{
      print(user.uid);
      return ClosetList();
    }
  }
}
