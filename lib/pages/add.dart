import 'dart:io';

import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:meu_closet/models/my_user.dart';
import 'package:meu_closet/services/database.dart';
import 'package:provider/provider.dart';

class Add extends StatefulWidget {
  @override
  _AddState createState() => _AddState();
}

class _AddState extends State<Add> {

  String uidOwner = '';
  String description = '';
  String category = '';
  String sellPrice = '';
  String buyPrice = '';

  File _image;
  String valueChoose;
  String civilState;

  _imgFromCamera() async {
    File image = await ImagePicker.pickImage(source: ImageSource.camera, imageQuality: 50);

    setState(() {
      _image = image;
    });
  }

  _imgFromGallery() async {
    File image = await ImagePicker.pickImage(source: ImageSource.gallery, imageQuality: 50);

    setState(() {
      _image = image;
    });
  }

  void _showPicker(context) {




    showModalBottomSheet(
        context: context,
        builder: (BuildContext bc) {
          return SafeArea(
            child: Container(
              child: new Wrap(
                children: <Widget>[
                  new ListTile(
                      leading: new Icon(Icons.photo_library),
                      title: new Text('Photo Library'),
                      onTap: () {
                        _imgFromGallery();
                        Navigator.of(context).pop();
                      }),
                  new ListTile(
                    leading: new Icon(Icons.photo_camera),
                    title: new Text('Camera'),
                    onTap: () {
                      _imgFromCamera();
                      Navigator.of(context).pop();
                    },
                  ),
                ],
              ),
            ),
          );
        });
  }

  @override
  Widget build(BuildContext context) {

    final user = Provider.of<MyUser>(context);
    String userUID = user.uid;

    return Scaffold(
      appBar: AppBar(
        title: Text('Add'),
      ),
      body: SingleChildScrollView(
        child: Container(
          padding: EdgeInsets.all(30),
          child: Column(
            children: [
              Center(
                child: Container(
                  margin: EdgeInsets.all(20),
                  child: GestureDetector(
                    onTap: () {
                      _showPicker(context);
                    },
                    child: CircleAvatar(
                      radius: 60,
                      backgroundColor: Color(0xffFDCF09),
                      child: _image != null
                          ? ClipRRect(
                        borderRadius: BorderRadius.circular(50),
                        child: Image.file(
                          _image,
                          width: 110,
                          height: 110,
                          fit: BoxFit.cover,
                        ),
                      )
                          : Container(
                        decoration: BoxDecoration(color: Colors.grey[200], borderRadius: BorderRadius.circular(50)),
                        width: 110,
                        height: 110,
                        child: Icon(
                          Icons.camera_alt,
                          color: Colors.grey[800],
                        ),
                      ),
                    ),
                  ),
                ),
              ),
              TextField(
                onChanged: (val) {
                  setState(() {
                    description = val;
                  });
                },
                enableSuggestions: false,
                autocorrect: false,
                decoration: InputDecoration(
                  hintText: 'Digitar descrição',
                  focusColor: Colors.blue[900],
                ),
              ),
              DropdownButton(
                  hint: Text(
                    'Selecione',
                    style: TextStyle(
                      color: Colors.grey[600],
                    ),
                  ),
                  value: valueChoose,
                  items: [
                    'Blusa',
                    'Meia',
                    'Calça',
                    'Shorts',
                    'Casaco',
                    'Luva',
                    'Óculos',
                    'Chapéu',
                  ].map((value) {
                    return DropdownMenuItem(
                      value: value,
                      child: Text(value),
                    );
                  }).toList(),
                  //dropdownColor: Colors.blue,
                  icon: Icon(Icons.arrow_drop_down),
                  isExpanded: true,
                  style: TextStyle(
                    color: Colors.black,
                    //fontSize: 22
                  ),
                  onChanged: (value) {
                    setState(() {
                      valueChoose = value;
                      category = valueChoose;
                    });
                  }),
              Row(
                children: [
                  Expanded(
                      child: TextField(
                        onChanged: (val) {
                          setState(() {
                            sellPrice = val;
                          });
                        },
                        enableSuggestions: false,
                        autocorrect: false,
                        keyboardType: TextInputType.numberWithOptions(decimal: true),
                        decoration: InputDecoration(
                          hintText: 'Preço de venda',
                          focusColor: Colors.blue[900],
                        ),
                      ),
                  ),
                  Expanded(
                    child: TextField(
                      onChanged: (val) {
                        setState(() {
                          buyPrice = val;
                        });
                      },
                      enableSuggestions: false,
                      autocorrect: false,
                      keyboardType: TextInputType.numberWithOptions(decimal: true),
                      decoration: InputDecoration(
                        hintText: 'Preço de compra',
                        focusColor: Colors.blue[900],
                      ),
                    ),
                  ),
                ],
              ),
              SizedBox(
                height: 10,
              ),
              FlatButton(
                  onPressed: () async {
                    dynamic result = await DatabaseService(uid: userUID).addItem('picture', description, category, sellPrice, buyPrice);
                    Navigator.pop(context);
                  },
                  child: Row(
                    children: [
                      Text(
                          'Adicionar',
                        style: TextStyle(
                          color: Colors.white
                        ),
                      ),
                      Icon(
                          Icons.add,
                          color: Colors.white
                      ),
                    ],
                  ),
                  height: 50,
                  color: Colors.purple,
              ),
            ],
          ),
        ),
      ),
    );
  }
}
