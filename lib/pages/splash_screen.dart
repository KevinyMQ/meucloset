import 'package:flutter/material.dart';

class SplashScreen extends StatefulWidget {
  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.purple,
      body: SafeArea(
        child: GestureDetector(
          onTap: (){
              print('clicou');
              Navigator.pushNamedAndRemoveUntil(context,'/auth_wrapper', (Route<dynamic> route) => false);
            },
          child: Container(
            height: double.infinity,
            child: Center(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text(
                      'Meu',
                      style: TextStyle(
                        color: Colors.white,
                        fontSize: 40,
                        fontWeight: FontWeight.w100
                      ),
                    ),
                    Text(
                      'Closet',
                      style: TextStyle(
                          color: Colors.white,
                          fontSize: 40,
                          fontWeight: FontWeight.w200
                      ),
                    ),
                  ],
                )
              ),
          ),
        ),
      ),
    );
  }
}
