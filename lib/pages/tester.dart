import 'package:flutter/material.dart';

class Tester extends StatefulWidget {
  @override
  _TesterState createState() => _TesterState();
}

class _TesterState extends State<Tester> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              FlatButton(
                  onPressed: (){
                    Navigator.pushNamed(context, '/');
                  },
                  child: Text('SplashScreen'),
                  color: Colors.blue,
              ),
              FlatButton(
                onPressed: (){
                  Navigator.pushNamed(context, '/login');
                },
                child: Text('Login'),
                color: Colors.blue,
              ),
              FlatButton(
                onPressed: (){
                  Navigator.pushNamed(context, '/register');
                },
                child: Text('Register'),
                color: Colors.blue,
              ),
              FlatButton(
                onPressed: (){
                  Navigator.pushNamed(context, '/closet_list');
                },
                child: Text('ClosetList'),
                color: Colors.blue,
              ),
              FlatButton(
                onPressed: (){
                  Navigator.pushNamed(context, '/add');
                },
                child: Text('Add'),
                color: Colors.blue,
              ),
            ],
          ),
        ),
      ),
    );
  }
}
