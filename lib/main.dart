import 'package:flutter/material.dart';
import 'package:meu_closet/services/auth.dart';

import 'package:provider/provider.dart';

import 'package:firebase_core/firebase_core.dart';
import 'package:meu_closet/pages/auth_wrapper.dart';
import 'package:meu_closet/pages/add.dart';
import 'package:meu_closet/pages/closet_list.dart';
import 'package:meu_closet/pages/login.dart';
import 'package:meu_closet/pages/register.dart';
import 'package:meu_closet/pages/splash_screen.dart';
import 'package:meu_closet/pages/tester.dart';

import 'models/my_user.dart';

void main() async {

  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();

  runApp(StreamProvider<MyUser>.value(
      value: AuthService().user,
      child: MaterialApp(
          theme: ThemeData(
            primaryColor: Colors.purple,
          ),
          // debugShowCheckedModeBanner: false,
          // localizationsDelegates: [
          //   //GlobalMaterialLocalizations.delegate,
          //   //GlobalWidgetsLocalizations.delegate
          // ],
          //supportedLocales: [const Locale('pt', 'BR')],
          initialRoute: '/',
          routes: {
            '/': (context) => SplashScreen(),
            '/auth_wrapper': (context) => AuthWrapper(),
            '/login': (context) => Login(),
            '/register': (context) => Register(),
            '/closet_list': (context) => ClosetList(),
            '/add': (context) => Add(),
            '/tester': (context) => Tester(),
          })
      )
  );
}

