import 'package:meu_closet/models/my_user.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'database.dart';

class AuthService {

  final FirebaseAuth _auth = FirebaseAuth.instance;

  // create user obj based on firebase user
  MyUser _userFromFirebaseUser(User user){
    return user != null ? MyUser(uid: user.uid) : null;
  }

  //auth change user stream
  Stream<MyUser> get user {
    return _auth.authStateChanges()
        .map((User user) => _userFromFirebaseUser(user));
  }

  // sign in with email & password
  Future signInWithEmailAndPassword(String email, String password) async {
    try{
      UserCredential result = await _auth.signInWithEmailAndPassword(email: email, password: password );
      User user = result.user;
      print(user.uid);
      return _userFromFirebaseUser(user);
    }catch(e){
      String error = '';
      if(e.hashCode == 360587416){
        error = 'Email inválido.';
      }else if(e.hashCode == 505284406){
        error = 'Nenhum registro encontrado.';
      }else if(e.hashCode == 185768934){
        error = 'Nenhum registro encontrado.';
      }else if(e.hashCode == 286357906){
        error = 'Não foi possível estabelecer conexão.';
      }
      return error;
    }
  }

  //register with email & password
  Future registerWithEmailAndPassword(String email, String password, String address, String picture, String phone) async {
    try{
      UserCredential result = await _auth.createUserWithEmailAndPassword(email: email, password: password);
      User user = result.user;
      DatabaseService(uid: user.uid).updateUserInfo(email, password, address, picture, phone);
      return _userFromFirebaseUser(user);
    }catch(e){
      print(e.toString());
      return null;
    }
  }

  //sign out
  Future signOut() async{
    try{
      return await _auth.signOut();
    }catch(e){
      print(e.toString());
      return null;
    }
  }
}