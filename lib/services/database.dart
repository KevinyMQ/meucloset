// import 'package:meu_closet/data/question_item.dart';
// import 'package:meu_closet/data/results_analyser.dart';
// import 'package:meu_closet/data/saves.dart';
import 'package:meu_closet/models/item.dart';
import 'package:meu_closet/models/my_user.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

class DatabaseService {
  final String uid;
  DatabaseService({this.uid});
  bool isLoad = false;

  //collection reference
  final CollectionReference smeCollection =
  FirebaseFirestore.instance.collection('sme');
  final CollectionReference historyCollection =
  FirebaseFirestore.instance.collection('history');

  final CollectionReference userCollection = FirebaseFirestore.instance.collection('users');
  final CollectionReference itemCollection = FirebaseFirestore.instance.collection('items');


  final CollectionReference incrementalCollection =
  FirebaseFirestore.instance.collection('incremental');
  final CollectionReference answers1Collection =
  FirebaseFirestore.instance.collection('answers1');
  final CollectionReference answers2Collection =
  FirebaseFirestore.instance.collection('answers2');

  Future<int> getCurrentTotalUsers() async {
    int snapshot;
    await incrementalCollection
        .doc("JiBw8jCNCTeDXHBDxBFl")
        .get()
        .then((querySnapshot) {
      snapshot = querySnapshot.get('id_users_counter');
    });
    return snapshot;
  }

  Future<Item> getItemSearch(String code) async {
    Item item = Item();
    // item.description = '1';
    // item.id = 'sFJx7tO5xgU1QvtiYlkM';
    // item.category = 'saddsasaddsa';
    await itemCollection
        .doc("$code")
        .get()
        .then((querySnapshot) {
          item.id = code;
          item.picture = querySnapshot.get('picture');
          item.category = querySnapshot.get('category');
          item.description = querySnapshot.get('description');
          item.sellPrice = querySnapshot.get('sellPrice');
          item.buyPrice = querySnapshot.get('buyPrice');
          item.uidOwner = querySnapshot.get('uidOwner');
        });

      return item;

  }

  // Future uploadImageToFirebase(BuildContext context, File _imageFile) async {
  //   String fileName = basename(_imageFile.path);
  //   StorageReference firebaseStorageRef =
  //   FirebaseStorage.instance.ref().child('uploads/$fileName');
  //   StorageUploadTask uploadTask = firebaseStorageRef.putFile(_imageFile);
  //   StorageTaskSnapshot taskSnapshot = await uploadTask.onComplete;
  //   taskSnapshot.ref.getDownloadURL().then(
  //         (value) => print("Done: $value"),
  //   );
  // }


  int result(List<int> varis, List<int> answers) {
    int tt = 0;
    for (int i = 0; i < varis.length; i++) {
      for (int a = 0; a < answers.length; a++) {
        if (varis[i] - 1 == a) {
          tt = tt + answers[i];
        }
      }
    }
    return tt;
  }

  Future<List<int>> getAnswers(int type, String answersId) async {
    List<int> answers = [];
    if (type == 1) {
      await answers1Collection.doc(answersId).get().then((querySnapshot) {
        for (int i = 0; i < 21; i++) {
          answers.add(querySnapshot.get(i.toString()));
        }
      });
    } else {
      await answers2Collection.doc(answersId).get().then((querySnapshot) {
        for (int i = 0; i < 7; i++) {
          answers.add(querySnapshot.get(i.toString()));
        }
      });
    }
    return answers;
  }

  // Future<List<Saves>> getHistory(String initialDate, String finalDate) async {
  //   List<Saves> savings = await [];
  //
  //   await historyCollection.get().then((querySnapshot) async {
  //     for (int i = 0; i < querySnapshot.docs.length; i++) {
  //       List<int> answers = [];
  //       answers = await getAnswers(querySnapshot.docs[i].data()['type'],
  //           querySnapshot.docs[i].data()['answers_id']);
  //       Saves saves = Saves();
  //       saves.type = querySnapshot.docs[i].data()['type'];
  //       saves.date = querySnapshot.docs[i].data()['date'];
  //       saves.points = [
  //         result(rdepressao, answers),
  //         result(ransiedade, answers),
  //         result(restresse, answers)
  //       ];
  //       savings.add(saves);
  //       //print(savings);
  //     }
  //   });
  //   return savings;
  // }
  //
  // Future addHistory(List<QuestionItem> questionsToSave) async {
  //   String dateNOW = new DateTime.now().toString().substring(0, 10);
  //
  //   if (questionsToSave.length == 21) {
  //     return await answers1Collection.add({
  //       '0': questionsToSave[0].answer,
  //       '1': questionsToSave[1].answer,
  //       '2': questionsToSave[2].answer,
  //       '3': questionsToSave[3].answer,
  //       '4': questionsToSave[4].answer,
  //       '5': questionsToSave[5].answer,
  //       '6': questionsToSave[6].answer,
  //       '7': questionsToSave[7].answer,
  //       '8': questionsToSave[8].answer,
  //       '9': questionsToSave[9].answer,
  //       '10': questionsToSave[10].answer,
  //       '11': questionsToSave[11].answer,
  //       '12': questionsToSave[12].answer,
  //       '13': questionsToSave[13].answer,
  //       '14': questionsToSave[14].answer,
  //       '15': questionsToSave[15].answer,
  //       '16': questionsToSave[16].answer,
  //       '17': questionsToSave[17].answer,
  //       '18': questionsToSave[18].answer,
  //       '19': questionsToSave[19].answer,
  //       '20': questionsToSave[20].answer,
  //     }).then((docRef) async {
  //       await historyCollection.add({
  //         'type': 1,
  //         'user_uid': uid,
  //         'date': dateNOW,
  //         'answers_id': docRef.id,
  //         '1': result(rdepressao, questionsToSave.map((e) => e.answer).toList()),
  //         '2': result(ransiedade, questionsToSave.map((e) => e.answer).toList()),
  //         '3': result(restresse, questionsToSave.map((e) => e.answer).toList()),
  //       });
  //     });
  //   } else {
  //     return await answers2Collection.add({
  //       '0': questionsToSave[0].answer,
  //       '1': questionsToSave[1].answer,
  //       '2': questionsToSave[2].answer,
  //       '3': questionsToSave[3].answer,
  //       '4': questionsToSave[4].answer,
  //       '5': questionsToSave[5].answer,
  //       '6': questionsToSave[6].answer,
  //     }).then((docRef) async {
  //       await historyCollection.add({
  //         'type': 2,
  //         'user_uid': uid,
  //         'date': dateNOW,
  //         'answers_id': docRef.id,
  //         '1': questionsToSave[0].answer,
  //         '2': questionsToSave[1].answer,
  //         '3': questionsToSave[2].answer,
  //         '4': questionsToSave[3].answer,
  //         '5': questionsToSave[4].answer,
  //         '6': questionsToSave[5].answer,
  //         '7': questionsToSave[6].answer,
  //       });
  //     });
  //   }
  //
  // }

  // Future<List<int>> answers(int type, String answersId) async {
  //   List<int> answers = [];
  //   if (type == 1) {
  //     await answers1Collection.doc(answersId).get().then((querySnapshot) async {
  //       for (int i = 0; i < 21; i++) {
  //         answers.add(querySnapshot.get(i.toString()));
  //       }
  //       print(answers);
  //     });
  //   }
  //   return answers;
  // }


  Future addItem(String picture, String description, String category, String sellPrice, String buyPrice) async {
      String dateNow = new DateTime.now().toString().substring(0, 10);

      return await itemCollection.add({
        'uidOwner': uid,
        'picture': picture,
        'date': dateNow,
        'category': category,
        'description': description,
        'sellPrice': sellPrice,
        'buyPrice': buyPrice,

      }).then((docRef) async {

      });
    }


  Future updateUserInfo(String email, String password,
      String address, String picture, String phone) async {
    return await userCollection.doc(uid).set({
      'email': email,
      'password': password,
      'address': address,
      'picture': picture,
      'phone': phone,
    });
  }

  List<Item> _itemsFromFirebaseSnapshot(QuerySnapshot snapshot) {
    return snapshot.docs.map((doc) {

      Item item = Item();
      item.id = doc.id;
      item.category = doc.data()['category'];
      item.date = doc.data()['date'];
      item.description = doc.data()['description'];
      item.picture = doc.data()['picture'];
      item.sellPrice = doc.data()['sellPrice'];
      item.buyPrice = doc.data()['buyPrice'];
      item.uidOwner = doc.data()['uidOwner'];

      return item;

    }).toList();
  }

  MyUser _userFromFirebaseUserUnique(DocumentSnapshot snapshot) {
    MyUser myUser = MyUser(uid: uid);
    myUser.email = snapshot.data()['email'];
    myUser.password = snapshot.data()['password'];
    myUser.picture = snapshot.data()['picture'];
    myUser.address = snapshot.data()['address'];
    myUser.phone = snapshot.data()['phone'];
    return myUser;
  }

  Stream<MyUser> get dbUsers {
    return userCollection.doc(uid).snapshots().map(_userFromFirebaseUserUnique);
  }

  Stream<MyUser> get itemSearch {
    return itemCollection.doc(uid).snapshots().map(_userFromFirebaseUserUnique);
  }

  Stream<List<Item>> get items {
    return itemCollection
        .orderBy('date', descending: true)
        .snapshots()
        .map(_itemsFromFirebaseSnapshot);
  }

}